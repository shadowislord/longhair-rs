#[macro_use]
extern crate criterion;
extern crate aligned_alloc;
extern crate longhair_rs;
extern crate rand;

mod aligned_buf;

use crate::aligned_buf::AlignedBuffer;
use criterion::{Criterion, ParameterizedBenchmark, Throughput};
use longhair_rs::Cauchy;
use rand::{thread_rng, Rng};
use std::fmt;

fn generate_blocks(block_size: usize, num_blocks: u32) -> Vec<AlignedBuffer> {
    let mut blocks = Vec::with_capacity(num_blocks as usize);
    for _ in 0..num_blocks {
        let mut block = AlignedBuffer::new(block_size);
        thread_rng().fill(&mut block[..]);
        blocks.push(block);
    }
    blocks
}

fn empty_blocks(block_size: usize, num_blocks: u32) -> Vec<AlignedBuffer> {
    let mut blocks = Vec::with_capacity(num_blocks as usize);
    for _ in 0..num_blocks {
        blocks.push(AlignedBuffer::new(block_size));
    }
    blocks
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct Params {
    block_size: usize,
    input_blocks: u32,
    output_blocks: u32,
}

impl From<(usize, u32, u32)> for Params {
    fn from(other: (usize, u32, u32)) -> Params {
        Params {
            block_size: other.0,
            input_blocks: other.1,
            output_blocks: other.2,
        }
    }
}

impl fmt::Debug for Params {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}:{}/{}",
            self.input_blocks, self.output_blocks, self.block_size
        )
    }
}

fn create_bench<I, T>(params: I) -> ParameterizedBenchmark<Params>
where
    T: Into<Params>,
    I: IntoIterator<Item = T>,
{
    ParameterizedBenchmark::new(
        "encode",
        move |b, params: &Params| {
            let mut cauchy = Cauchy::new(params.input_blocks);
            let input = generate_blocks(params.block_size, params.input_blocks);
            let output = empty_blocks(params.block_size, params.output_blocks);
            b.iter_with_setup(
                || output.clone(),
                |mut output| cauchy.encode(&input, &mut output),
            );
        },
        params.into_iter().map(|p| p.into()),
    )
    .throughput(move |params| Throughput::Bytes((params.block_size * params.input_blocks as usize) as u64))
}

fn encode(c: &mut Criterion) {
    // let params = vec![(512, 16, 8), (576, 16, 8), (640, 16, 8), (1024, 16, 8)];
    let params = vec![
        (1472, 6, 3),
        (1472, 8, 4),
        (1472, 8, 6),
        (1472, 10, 6),
        (1472, 12, 6),
        (1472, 16, 8),
        (1472, 20, 10),
        (1472, 20, 14),
    ];
    c.bench("longhair", create_bench(params));
}

criterion_group!(benches, encode);
criterion_main!(benches);
