use aligned_alloc::{aligned_alloc, aligned_free};
use std::borrow::{Borrow, BorrowMut};
use std::ops::{Deref, DerefMut};
use std::ptr;
use std::slice;

#[derive(Debug)]
pub struct AlignedBuffer {
    data: *mut u8,
    size: usize,
}

const ALIGN: usize = 32;

impl AlignedBuffer {
    // fn new(size: usize, align: usize) -> AlignedBuffer {
    //     let data = aligned_alloc(size, align) as *mut _;
    //     AlignedBuffer { data, size }
    // }

    pub fn new(size: usize) -> AlignedBuffer {
        assert_eq!(size % ALIGN, 0);
        let data = aligned_alloc(size, ALIGN) as *mut _;
        AlignedBuffer { data, size }
    }
}

impl Deref for AlignedBuffer {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        unsafe { slice::from_raw_parts(self.data, self.size) }
    }
}

impl DerefMut for AlignedBuffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { slice::from_raw_parts_mut(self.data, self.size) }
    }
}

impl Drop for AlignedBuffer {
    fn drop(&mut self) {
        unsafe {
            aligned_free(self.data as *mut _);
        }
    }
}

impl Borrow<[u8]> for AlignedBuffer {
    fn borrow(&self) -> &[u8] {
        self.deref()
    }
}

impl BorrowMut<[u8]> for AlignedBuffer {
    fn borrow_mut(&mut self) -> &mut [u8] {
        self.deref_mut()
    }
}

impl Clone for AlignedBuffer {
    fn clone(&self) -> AlignedBuffer {
        let b = AlignedBuffer::new(self.size);
        unsafe {
            ptr::copy_nonoverlapping(self.data, b.data, self.size);
        }
        b
    }
}

impl PartialEq for AlignedBuffer {
    fn eq(&self, other: &AlignedBuffer) -> bool {
        self[..] == other[..]
    }
}
