# Seeds for failure cases proptest has generated in the past. It is
# automatically read and these particular cases re-run before any
# novel cases are generated.
#
# It is recommended to check this file in to source control so that
# everyone who runs the test benefits from these saved cases.
xs 3520976009 3136425728 3856641602 618106970 # shrinks to x = 0, y = 0
xs 3110758828 606197675 848889457 149601591 # shrinks to block_size = 848, input_blocks = 1, output_blocks = 1
xs 2620221130 3946762630 733918366 4291753880 # shrinks to block_size = 8, input_blocks = 1, output_blocks = 1
xs 3564718483 1530928513 175141478 1165762524 # shrinks to block_size = 0, input_blocks = 1, output_blocks = 1
xs 1559936545 1661874765 1795601617 325512365 # shrinks to block_size = 8, input_blocks = 2, output_blocks = 2
xs 2002756377 903968415 181003163 2828939448 # shrinks to block_size = 8, input_blocks = 3, output_blocks = 1
